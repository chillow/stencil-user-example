import { Component, Prop, Event, EventEmitter, State, Method } from "@stencil/core";

@Component({
    tag: 'crt-list',
    shadow: false
})
export class CrtList {
    
    @State()
    @Prop()
    data = ["Carat", "Data"];

    @Prop()
    text: string;

    @State()
    active: boolean;

    @Event()
    hoverEvent: EventEmitter

    @Event()
    selectData: EventEmitter

    @Method()
    selectDataHandler(data: any) {
        this.selectData.emit(data);
    }

    render() {
        let options = (this.text) ? this.text : this.data.map(current => (<li onClick={this.selectDataHandler.bind(this, current)}>{current}</li>));
        // console.log(this.data, options);

        return (<ul>{options}</ul>);
    }
}