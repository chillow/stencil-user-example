import { Component, Prop, Method, Event, EventEmitter } from "@stencil/core";

@Component({
    tag: 'crt-user-details',
    styleUrl: 'crt-user-details.styles.scss'
})
export class CrtUserDetails {

    @Prop()
    user: User;

    @Event()
    activateUser: EventEmitter;

    @Method()
    clickHandler() {
        this.activateUser.emit(this.user);
    }

    render() {
        return (
            <div class="container">
                <div>Username</div>
                <div>{this.user.username}</div>
                <div>Active</div>
                <div>{this.user.active + ''}</div>
                <div class="active-button" onClick={this.clickHandler.bind(this)}>Toggle Active</div>
                <img class="avatar" src={this.user.avatar}></img>
            </div>
        )
    }
}

export interface User {
    id: string,
    username: string,
    avatar: string,
    active: boolean
}
