import { Component, Prop, Event, EventEmitter, Listen, State } from "@stencil/core";

@Component({
    tag: 'crt-user',
    styleUrl: 'crt-user.styles.scss'
})
export class CrtUser {

    @Prop()
    users: User[] = defaultUsers;

    @State()
    selectedUser: User;

    @Event()
    onClick: EventEmitter;

    clickHandler(user: User) {
      this.selectedUser = {...user, ...{active: !user.active}};
      let userInList = this.findUser(user.username);
      userInList.active = !userInList.active;
    };

    @Listen('selectData')
    selectUser(user: CustomEvent) {
        this.selectedUser = this.findUser(user.detail);
    }

    @Listen('activateUser')
    activateUser(user: CustomEvent) {
      this.clickHandler(user.detail);
    }

    findUser(username: string) {
      return this.users.find(current => current.username === username)
    }

    render() {
        let names = this.users.map((currentUser: User) => currentUser.username);
        let details = this.selectedUser ? (<crt-user-details class="user-details" user={this.selectedUser}></crt-user-details>) : undefined;
        return [
            <crt-list class="userList" data={names}></crt-list>,
            details
        ]
    }
}

export const defaultUsers: User[] = [
    {
        id: "test",
        avatar: "https://api.adorable.io/avatars/285/test",
        username: "test",
        active: true,
      },{
        id: "test2",
        avatar: "https://api.adorable.io/avatars/285/test2",
        username: "test2",
        active: true
      },{
        id: "test3",
        avatar: "https://api.adorable.io/avatars/285/test3",
        username: "test3",
        active: false
      },{
        id: "test4",
        avatar: "https://api.adorable.io/avatars/285/test4",
        username: "test4",
        active: true
      }
];

export class UserImpl implements User {
    id: string;
    username: string;
    avatar: string;
    active: boolean;

    toggleActive() {
        this.active = !this.active;
    }
}

export interface User {
    id: string,
    username: string,
    avatar: string,
    active: boolean
}
