const sass = require('@stencil/sass');

exports.config = {
  bundles: [
    {components: ['crt-list', 'crt-user', 'crt-user-details']}
  ],
  plugins: [
    sass()
  ],
  namespace: 'mycomponent',
  outputTargets:[
    { 
      type: 'dist' 
    },
    { 
      type: 'www',
      serviceWorker: false
    }
  ]
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
}
